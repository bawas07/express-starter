require('module-alias/register');
// const path = require('path');
/* eslint-disable no-unused-vars */
const compress = require('compression');
const cors = require('cors');
const helmet = require('helmet');
const morgan = require('morgan');
const express = require('express');
const config = require('config');
const { ApolloServer } = require('apollo-server-express');
const { apolloUploadExpress } = require('apollo-upload-server');
const fs = require('fs');

const { authAdmin, notFound } = require('@helpers');
const routes = require('./src/routes');
const { events } = require('./src/eventemitter');
const schema = require('./src/schema');

const graphqlPath = '/graphql';

const app = express();

//Global Variable
global.emailErrorLog = fs.createWriteStream(
    __dirname + '/log/mailer.error.log',
    {
        flags: 'a'
    }
);

// Enable CORS, security, compression, favicon and body parsing
app.use(cors());
app.use(helmet());
app.use(compress());
const green = '\033[0;32m';
const noCollor = '\033[0m';
app.use(morgan(`${green}:method${noCollor} :url :status :res[content-length] - :response-time ms`));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// app.use(favicon(path.join(config.public), 'favicon.ico'));
// Host the public folder
// app.use('/', express.static(config.public));
app.use('/uploads', express.static(config.uploads));

// Event Listener
// events.UserRegistered.listenUserRegistered();
// events.UserRegistered.listenUserJoined();
// events.MemberLatePresence.listenMemberLate();
// events.MemberOverwork.listenMemberOverwork();

// API Version
app.use('/api/v1', routes.v1);

// Graphql End point
// const server = new ApolloServer(schema);
// app.use(graphqlPath, authAdmin, apolloUploadExpress({ maxFileSize: 6000000 }));
// server.applyMiddleware({ app, graphqlPath });

app.use(notFound());

module.exports = app;
