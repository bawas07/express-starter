module.exports = {
    host: 'localhost',
    port: 3333,
    public: 'public',
    uploads: 'public/uploads',
    paginate: {
        default: 10,
        max: 50
    },
    authentication: {
        secret: 'sup3rs3cr3t',
        strategies: ['jwt', 'local'],
        path: '/authentication',
        service: 'users',
        jwt: {
            header: {
                typ: 'access'
            },
            audience: 'https://yourdomain.com',
            subject: 'anonymous',
            issuer: 'feathers',
            algorithm: 'HS256',
            expiresIn: '1d'
        },
        local: {
            entity: 'users',
            usernameField: 'email',
            passwordField: 'password'
        },
        facebook: {
            clientID: 'clientID',
            clientSecret: 'clientSecret',
            successRedirect: '/',
            scope: ['public_profile', 'email'],
            profileFields: [
                'id',
                'displayName',
                'first_name',
                'last_name',
                'email',
                'gender',
                'profileUrl',
                'birthday',
                'picture',
                'permissions'
            ],
            graphUri: 'https://graph.accountkit.com/v1.3/'
        },
        cookie: {
            enabled: true,
            name: 'feathers-jwt',
            httpOnly: false,
            secure: false
        }
    },
    mysql: 'mysql://root:root@127.0.0.1:3306/MyDB'
};
