const { auth } = require('@helpers');
const express = require('express');

const register = require('./register');
const login = require('./login');
const users = require('./users');

// Declare API Route and API Version
const v1 = express.Router();

v1.use('/register', register);
v1.use('/login', login);
v1.use('/users', auth, users);
v1.use('/', (req, res) => {
    res.send('Hello World');
});

module.exports = v1;
