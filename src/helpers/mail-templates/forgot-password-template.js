const config = require('config');

const forgotPasswordTemplate = info =>
/*eslint-disable indent */
  `
    <center>
    <img src="https://${config.host}/public/email/gajiandulu-email-header.jpg"/>
    <h2>Reset Password</h2>
    <p>Anda telah melakukan permintaan untuk reset password dan permintaan telah berhasil untuk mengganti password akun anda.</p>
    <h4>===================================================</h4>
    <p>Berikut adalah password baru anda: <b>${info.passgen}</b></p>
    <h4>===================================================</h4>
    <hr>
    <p>Jika kamu memiliki pertanyaan atau klarifikasi lebih lanjut, kamu dapat selalu menghubungi kami melalui:</p>
    <p>Email: cs@gajiandulu.id</p>
    <p>LINE ID: @gajiandulu</p>
    <br>
    <p>Terima kasih atas dukungan dan kepercayaan kakak menggunakan GajianDulu!</p>
    <center>
      <a href="https://www.facebook.com/gajiandulu" target="_blank" rel="noreferrer"><img src="https://${
        config.host
      }/public/email/facebook-icon.png"/></a>
      <a href="http://gajiandulu.id" target="_blank" rel="noreferrer"><img src="https://${
        config.host
      }/public/email/link-icon.png"/></a>
      <a href="https://www.instagram.com/gajiandulu" target="_blank" rel="noreferrer"><img src="https://${
        config.host
      }/public/email/instagram-icon.png"/></a>
    </center>
    <p>Copyright © 2018 PT. Bantu Tumbuh Bersama, All rights reserved.</p>
    <p>Our mailing address is:</p>
    <p>cs@gajiandulu.id</p>
    </center>
  `;

module.exports = forgotPasswordTemplate;
