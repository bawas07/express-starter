const config = require('config');
const { dateHelper } = require('@helpers');

const presencesXls = ({ companyData, month, year }) =>
/*eslint-disable indent */
  `
  <center>
  <img src="https://${config.host}/public/email/gajiandulu-email-header.jpg"/>
  <h2>Data Presensi ${companyData.name}</h2>
  <p>Anda telah melakukan permintaan untuk request Data Presensi.</p>
  <p>Berikut adalah Data Presensi yang di export dalam bentuk excel di bulan</p>
  <h4>===================================================</h4>
  <h3> ${dateHelper[parseInt(month)]} ${year} </h3>
  <p>Lihat Attachment</p>
  <h4>===================================================</h4>
  <hr>
  <p>Jika kamu memiliki pertanyaan atau klarifikasi lebih lanjut, kamu dapat selalu menghubungi kami melalui:</p>
  <p>Email: cs@gajiandulu.id</p>
  <p>LINE ID: @gajiandulu</p>
  <br>
  <p>Terima kasih atas dukungan dan kepercayaan kakak menggunakan GajianDulu!</p>
  <center>
    <a href="https://www.facebook.com/gajiandulu" target="_blank" rel="noreferrer"><img src="https://${
      config.host
    }/public/email/facebook-icon.png"/></a>
    <a href="http://gajiandulu.id" target="_blank" rel="noreferrer"><img src="https://${
      config.host
    }/public/email/link-icon.png"/></a>
    <a href="https://www.instagram.com/gajiandulu" target="_blank" rel="noreferrer"><img src="https://${
      config.host
    }/public/email/instagram-icon.png"/></a>
  </center>
  <p>Copyright © 2018 PT. Bantu Tumbuh Bersama, All rights reserved.</p>
  <p>Our mailing address is:</p>
  <p>cs@gajiandulu.id</p>
  </center>
  `;

module.exports = presencesXls;
