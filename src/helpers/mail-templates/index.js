const forgotPasswordTemplate = require('./forgot-password-template');
const managerInviting = require('./manager-inviting');
const presencesXls = require('./presences-xls');

module.exports = {
    forgotPasswordTemplate,
    managerInviting,
    presencesXls
};
