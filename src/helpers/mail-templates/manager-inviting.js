const config = require('config');

const managerInviting = ({ companyData, data }) =>
/*eslint-disable indent */
  `
    <center>
    <img src="https://${config.host}/public/email/gajiandulu-email-header.jpg"/>
    <h1>Undangan Anggota ${companyData.name}</h1>
    <p>Anda mendapatkan undangan untuk bergabung dengan ${companyData.name}</p>
    <p>Manajer perusahaan telah mengundang anda dengan informasi berikut:</p><br>
    <p>Nama Lengkap : ${data.name}</p>
    <p>Email        : ${data.email}</p>
    <p>Telepon      : ${data.phone}</p><br>
    <p>Mohon lakukan register ulang di dalam aplikasi ponsel GajianDulu dengan alamat email harus sesuai data tertera di atas.</p>
    <p>Lalu, silakan anda masukkan nama kode perusahaan setelah melewati tahap registrasi ulang, dengan kode di bawah ini.</p>
    <p>--------------------------------------------------------------------------</p>
    <h2>${companyData.codename}</h2>
    <p>--------------------------------------------------------------------------</p>
    <hr>
    <p>Jika kamu memiliki pertanyaan atau klarifikasi lebih lanjut, kamu dapat selalu menghubungi kami melalui:</p>
    <p>Email: cs@gajiandulu.id</p>
    <p>LINE ID: @gajiandulu</p>
    <br>
    <p>Terima kasih atas dukungan dan kepercayaan kakak menggunakan GajianDulu!</p>
    <center>
      <a href="https://www.facebook.com/gajiandulu" target="_blank" rel="noreferrer"><img src="https://${
        config.host
      }/public/email/facebook-icon.png"/></a>
      <a href="http://gajiandulu.id" target="_blank" rel="noreferrer"><img src="https://${
        config.host
      }/public/email/link-icon.png"/></a>
      <a href="https://www.instagram.com/gajiandulu" target="_blank" rel="noreferrer"><img src="https://${
        config.host
      }/public/email/instagram-icon.png"/></a>
    </center>
    <p>Copyright © 2018 PT. Bantu Tumbuh Bersama, All rights reserved.</p>
    <p>Our mailing address is:</p>
    <p>cs@gajiandulu.id</p>
    </center>
  `;

module.exports = managerInviting;
