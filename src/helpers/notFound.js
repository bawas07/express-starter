/* eslint-disable no-unused-vars */
module.exports = () =>
    function(req, res, next) {
        res.status(404).send('Sorry can\'t find that!');
    };
