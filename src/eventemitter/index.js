const EventEmitter = require('events').EventEmitter;

let observe = new EventEmitter();

const events = {
};

module.exports = { observe, events };
